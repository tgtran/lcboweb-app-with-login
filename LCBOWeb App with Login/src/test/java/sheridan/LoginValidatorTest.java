package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	//tests "must have alphanumeric and digits in password but cannot start with a number"
	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "sh4r1dan" ));
	}
	
	@Test (expected = NumberFormatException.class)
	public void testIsValidLoginException() {
		assertTrue("Invalid login", LoginValidator.isValidLoginName("5her1dan"));
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "sh4r1d" ));
	}
	
	@Test
	public void testIsValidLoginBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("sheridan"));
	}
	
	/**tests to check if there password has at least 6 alphanumeric characters */
	@Test
	public void testIsValidLoginAlphaRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "sh4r1dan" ));
	}
	
	@Test (expected = NullPointerException.class)
	public void testIsValidLoginAlphaException() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("sher1dan"));
	}
	
	@Test
	public void testIsValidLoginAlphaBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "sh4r1d" ));
	}
	
	@Test
	public void testIsValidLoginAlphaBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.isValidLoginName("null"));
	}
	

}

package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		/*
		 * //regex to check valid username String regex = "^[A-Za-z0-9]{6}$";\
		 */
		
		//convert string to char
		char[] chars = loginName.toCharArray();
		
		// Check if login name doesn't start with a number
		char firstChar = chars[0];
		if (Character.isDigit(firstChar))
		{
			System.out.println("Login name is not valid");
			return false;
		}
				
		// Login name must contain at least 6 alphanumeric characters
		int counter = 0;
		for (char c: chars)
		{

			// Check if it's alphanumeric
			if (Character.isAlphabetic(c) || Character.isDigit(c))
			{
				counter++;
			}
			else
			{
				System.out.println("Login name is not valid");
				return false;
			}
				
		}
		
		if (counter < 6)
		{
			System.out.println("Login name is not valid");
			return false;
		}
		
		// If we manage to get to this point, then login is valid
		System.out.println("Login Name is valid");
		return true;
		
		
//		return loginName.length( ) > 4 ;
	}
}
